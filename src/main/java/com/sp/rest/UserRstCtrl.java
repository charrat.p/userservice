package com.sp.rest;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper; 
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;

import javax.websocket.server.PathParam;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.User;
import com.sp.service.UserService;

@RestController
public class UserRstCtrl {
	
    @Autowired
    UserService uService;

	
	@RequestMapping(method=RequestMethod.POST,value="/users")
	public String addUser(@RequestParam String nom, @RequestParam String prenom, @RequestParam String mdp, @RequestParam String idconnection, @RequestParam String mail) {
		User user = new User( nom,  prenom, 0, mdp, idconnection,mail);
		uService.addUser(user);
		// Astuce pour rediriger vers une page html
		return "<meta http-equiv=\"refresh\" content=\"0;URL=choixcartes.html?idconnection="+idconnection+"\">";
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/users/{id}")
	public User getUserById(@PathVariable String id) {
		 User u = uService.getUser(Integer.valueOf(id));
         return u;
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/users/{idconnection}")
	public User getUserByNom(@PathVariable String idconnection) {
		 User u = uService.getUser(idconnection);
         return u;
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/users/{id}/decklist/")
	public List<Integer> getDeckListByID(@RequestParam int idUser) {
		return uService.getUserDeckList(idUser);    
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/users")
	public List<User> getAllUser() {
         return uService.getAllUser();
	}

	@RequestMapping(method = RequestMethod.POST,value = "/users/connection",produces = MediaType.APPLICATION_JSON_VALUE)
	public  String verifUser(@RequestParam String idconnection, @RequestParam String mdp){
		User user = uService.getUser(idconnection, mdp);
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json="";
		try {
			json = ow.writeValueAsString(user);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(json);
		return json;
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/userstart")
	public String addUsersMultiple() {
		User user = new User("Paul", "Paul",0,"azerty123","Paul69420");
		uService.addUser(user);
		user = new User("Jean", "Martin",0,"motdepasse69","JM_la_street");
		uService.addUser(user);
		user = new User("Charrat", "Phillips",1000000,"Troplong111","Philou_du_38!");
		uService.addUser(user);
		user = new User("Kroky", "A",69,"TuMeTrOuvErAsPaS","Mexikroky");
		uService.addUser(user);
		return "Ajout des utilisateurs";
	}
	
//	@RequestMapping(method=RequestMethod.PUT,value="/usersaddcartes",produces = MediaType.APPLICATION_JSON_VALUE)
//	public void UpdateDeckListUser(@RequestParam String idconnection,@RequestParam int idcarteUne,@RequestParam int idcarteDeux, @RequestParam int idcarteTrois,@RequestParam int idcarteQuatre,@RequestParam int idcarteCinq) {
//		Gson g = new Gson();
//		Carte carteUn = g.fromJson(carteUne, Carte.class);
//		Carte carteD = g.fromJson(carteDeux, Carte.class);
//		Carte carteT = g.fromJson(carteTrois, Carte.class);
//		Carte carteQ = g.fromJson(carteQuatre, Carte.class);
//		Carte carteC = g.fromJson(carteCinq, Carte.class);
//		System.out.println(carteUn.toString());
//		System.out.println("Je test les cartes");
//		uService.UpdateDeckListUser(idconnection,carteUn,carteD,carteT,carteQ,carteC);
//	}
}
