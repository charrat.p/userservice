	 package com.sp.repository;

    import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sp.model.User;

    @RunWith(SpringRunner.class)
    @DataJpaTest
    public class UserRepositoryTest {

        @Autowired
        UserRepository urepo;

        @Before
        public void setUp() {
            urepo.save(new User("Paul", "Paul",0,"azerty123","Paul69420"));
        }

        @After
        public void cleanUp() {
            urepo.deleteAll();
        }

        @Test
        public void saveUser() {
            urepo.save(new User("Paul", "Paul",0,"azerty123","Paul69420"));
            assertTrue(true);
        }

        @Test
        public void saveAndGetUser() {
            urepo.deleteAll();
            urepo.save(new User("Paul", "Paul",0,"azerty123","Paul69420"));
            List<User> UserList = new ArrayList<>();
            urepo.findAll().forEach(UserList::add);
            assertTrue(UserList.size() == 1);
            assertEquals(UserList.get(0).getPortefeuille(), 0);
        }

        @Test
        public void getUser() {
            List<User> UserList = urepo.findByIdConnexion("Paul69420");
            assertTrue(UserList.size() == 1);
            assertEquals(UserList.get(0).getPortefeuille(), 0);
        }			

        @Test
        public void findByIdconnexion() {
           urepo.save(new User("Paul1", "Paul",0,"azerty123","Paul69420"));
           urepo.save(new User("Paul2", "Paul",1,"azerty123","Paul69420"));
           urepo.save(new User("Paul3", "Paul",2,"azerty123","Paul69420"));
           urepo.save(new User("Paul4", "Paul",3,"azerty123","Paul69420"));
           List<User> heroList = new ArrayList<>();
           urepo.findByIdConnexion("Paul69420").forEach(heroList::add);
           assertTrue(heroList.size() == 5);
        }
    }
	