package com.sp.service;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sp.model.User;
import com.sp.repository.UserRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserService.class)
public class UserServiceTest {

	@Autowired
	private UserService hService;

	@MockBean
	private UserRepository hRepo;
	
	User tmpUser=new User("Paul1", "Paul",0,"azerty123","Paul69420");
	@Test
	public void getHero() {
		Mockito.when(
				hRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpUser));
		User UserInfo=hService.getUser(45);
		assertTrue(UserInfo.toString().equals(tmpUser.toString()));
	}
}
