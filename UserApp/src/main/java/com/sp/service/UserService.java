package com.sp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;

import com.sp.model.User;
import com.sp.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	
	public void addUser(User u) {
		User createdUser=userRepository.save(u);
		System.out.println(createdUser);
	}
	
	public User getUser(int id) {
		Optional<User> hOpt =userRepository.findById(id);
		if (hOpt.isPresent()) {
			return hOpt.get();
		}else {
			return null;
		}
	}
	
	public List<Integer> getUserDeckList(int idUser) {
		List<User> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);
		for (int i=0;i<users.size();i++) {
			User userTest = users.get(i);
			if(userTest.getIdConnexion().equals(idUser)) {
				return userTest.getListeCartes();
			}
		}
			return null;
	}
	
	public User getUser(String idconnection) {
		List<User> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);
		for (int i=0;i<users.size();i++) {
			User userTest = users.get(i);
			if(userTest.getIdConnexion().equals(idconnection)) {
				return userTest;
			}
		}
		return null;
	}
	
	public List<User> getAllUser() {
		List<User> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);
	    return users;

	}
	
	public User getUser(String idConnexion, String mdp) {
		User userReturn = new User("Inconnu", "Inconnu", 0, "Inconnu", "Inconnu");
		List<User> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);
		for (int j=0;j<users.size();j++) {
			User utest = users.get(j);
			if (utest.getIdConnexion().equals(idConnexion) && utest.getMdp().equals(mdp)) userReturn = utest;
		}
	    return userReturn;
	}
	
	public void createDeck(User user){
		List <Integer> listeidcartes = new ArrayList<>();
		user.setListeCartes(listeidcartes);
	}
	
	public void addCarte(User user, int idC) { //user or id?
		user.getListeCartes().add(idC);
	}
	
	public void removeCarte(User user, int idC) {//meme question
		user.getListeCartes().remove(idC);
	}
	
	public List <Integer> getFiveCards(int id){
		List <Integer> listId = new ArrayList<>();
		CarteListe response =  new RestTemplate().getForObject("http://localhost:8082/cartes/getFiveCards", CarteListe.class);
		for (int i=0; i< response.getCartes().size(); i++) {
			CarteDTO carte = response.getCartes().get(i);
			listId.add(carte.getCarte());
		}
		return listId;
	}
	
	public int getTransaction(Integer idT) {
		ResponseEntity<TransactionDTO> resp = new RestTemplate().getForEntity("http://localhost:8083/transaction/"+idT, TransactionDTO.class);
		return resp.getBody().getTransaction();
	}
	
	public int getCarte(Integer idC) {
		ResponseEntity<CarteDTO> resp = new RestTemplate().getForEntity("http://localhost:8082/carte/"+idC, CarteDTO.class);
		return resp.getBody().getCarte();
	}
	
	public static class TransactionDTO{
		public int idtransaction;
		
		public TransactionDTO() {}
		
		public int getTransaction() {
			return idtransaction;
		}
		
		public void setTransaction(int idT) {
			this.idtransaction = idT;
		}
	}
	
	public static class CarteDTO{
		public int idcarte;

		public CarteDTO() {}
		
		public int getCarte() {
			return idcarte;
		}
		
		public void setCarte(int idC) {
			this.idcarte = idC;
		}
	}
	public class CarteListe{
		private List<CarteDTO> Cartes;
		
		public CarteListe() {
			Cartes = new ArrayList<>();
		}

		public List<CarteDTO> getCartes() {
			return Cartes;
		}

		public void setCartes(List<CarteDTO> cartes) {
			Cartes = cartes;
		}
		
		
	}
}
